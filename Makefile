help: ## This help information
	@fgrep -h "##" $(MAKEFILE_LIST) | fgrep -v fgrep | sed -e 's/\\$$//' | sed -e 's/##//'

clean: ## Clean test, coverage and cache files
	@find . -name "*.pyc" | xargs rm -rf
	@find . -name "*.pyo" | xargs rm -rf
	@find . -name "__pycache__" -type d | xargs rm -rf
	@rm -f .coverage
	@rm -rf htmlcov/
	@rm -f coverage.xml
	@rm -f *.log
	@rm -rf docs/build/

test: clean ## Run tests
	@py.test -x django/favourites

test-debug: clean ## Run tests with debug enabled
	@py.test --ipdb -x django/favourites

test-matching: clean ## Run tests with keyword
	@py.test --ipdb django/favourites -k $(k)

flake8: ## Run Flake8
	@flake8 --show-source .

coverage: clean ## Build Coverage files
	@py.test -x --cov django/favourites --cov-config=.coveragerc --cov-report=term django/favourites --cov-report=html --cov-report=xml

detect-migrations: ## Check for new migrations
	@django/manage.py makemigrations --dry-run --noinput --settings=favourites.settings.test | grep 'No changes detected' -q || (echo 'Missing migration detected!' && exit 1)

dependencies: ## Install dependencies
	@pip install -U -r django/requirements/development.txt

database-tables: ## Run migrations
	@django/manage.py migrate --settings=favourites.settings.development

makemigrations: ## Create new migrations
	@django/manage.py makemigrations --settings=favourites.settings.development

superuser: ## Create superuser
	@django/manage.py createsuperuser --settings=favourites.settings.development

run: ## Run local server
	@django/manage.py runserver --settings=favourites.settings.development

shell: ## Run django shell for project
	@django/manage.py shell --settings=favourites.settings.development

check-python-import: ## Check for import errors
	@isort --check

fix-python-import: ## Fix import errors
	@isort -rc .

lint: clean flake8 check-python-import ## Run linter and look for import errors

outdated: ## Show outdated dependencies
	@pip list --outdated --format=columns