import os

import pytest
import requests

from django.test import override_settings
from favourites.extensions.products_api.client import ProductClient

from favourites.utils.vcr import get_vcr_instance

records_dir = os.path.join(os.path.dirname(__file__), 'records')
vcr = get_vcr_instance(records_dir)


@vcr.use_cassette
def test_should_found_valid_product():
    product_id = '3dd993ed-699f-d509-e07f-29aac25120ee'
    assert ProductClient.check_product_exists_on_api(product_id)


def test_should_not_found_valid_product():
    product_id = 'nao_existe_esse_produto'
    with pytest.raises(requests.exceptions.HTTPError):
        ProductClient.check_product_exists_on_api(product_id)


@override_settings(INTEGRATIONS={
    'products_api': {
        'url': 'fake_url',
    }
})
def test_should_raise_exception():
    product_id = '3dd993ed-699f-d509-e07f-29aac25120ee'
    with pytest.raises(Exception):
        ProductClient.check_product_exists_on_api(product_id)
