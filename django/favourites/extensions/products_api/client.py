import logging

import requests
from requests import Timeout

from django.conf import settings

logger = logging.getLogger(__name__)


class ProductClient:
    @staticmethod
    def check_product_exists_on_api(product_id):
        url = (
            f"{settings.INTEGRATIONS['products_api']['url']}"
            f"/{product_id}"
        )
        try:
            response = requests.get(url)
            response.raise_for_status()
            return True
        except Timeout as error:
            logger.error(
                f'Could not retrieve product {product_id} '
                f'due to a timeout {error}'
            )
            raise Timeout()
        except requests.exceptions.HTTPError as error:
            logger.error(
                f'Request Error when retrieving {product_id} '
                f'Error {error}'
            )
            raise
        except Exception as error:
            logger.error(
                f'Could not retrieve product {product_id} '
                f'due to a error {error}'
            )
            raise
