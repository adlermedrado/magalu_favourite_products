from rest_framework import serializers

from favourites.extensions.products_api.client import ProductClient
from favourites.products.models import Product


class ProductSerializer(serializers.ModelSerializer):
    class Meta:
        model = Product
        fields = '__all__'

    def check_product_availability_to_save(self, product_uuid, customer):
        if not Product.product_already_on_list(product_uuid, customer):
            ProductClient.check_product_exists_on_api(
                product_uuid
            )
