import pytest
from model_mommy import mommy

from favourites.products.models import Product


@pytest.mark.django_db
def test_product_already_exists_on_list(customer):
    product_external_id = '3dd993ed-699f-d509-e07f-29aac25120ee'
    mommy.make(
        'products.Product',
        customer=customer,
        external_id=product_external_id
    )

    assert Product.product_already_on_list(product_external_id, customer.id)


@pytest.mark.django_db
def test_product_dont_exists_on_list(customer):
    product_external_id = '3dd993ed-699f-d509-e07f-29aac25120ee'
    assert not Product.product_already_on_list(
        product_external_id,
        customer.id
    )
