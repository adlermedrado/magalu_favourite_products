from unittest import mock

import pytest
from requests import HTTPError
from rest_framework import status


@pytest.mark.django_db
def test_should_return_empty_list_of_products(client, url_products):
    url_products = f'{url_products}171/'
    response = client.get(url_products, format='json')

    assert response.status_code == status.HTTP_200_OK
    assert response.data['count'] == 0


@pytest.mark.django_db
def test_should_return_list_of_products(
    client,
    url_products,
    customer,
    product_for_customer
):
    url_products = f'{url_products}{customer.id}/'
    response = client.get(url_products, format='json')

    assert response.status_code == status.HTTP_200_OK
    assert response.data['count'] == 1
    assert response.data['results'][0]['title'] == product_for_customer.title


@pytest.mark.django_db
def test_should_save_new_favourite_for_customer(
    client,
    url_products,
    product_payload
):
    response = client.post(url_products, data=product_payload)
    assert response.status_code == status.HTTP_201_CREATED


@pytest.mark.django_db
def test_should_not_save_when_customer_already_has_saved_product_before(
    client,
    url_products,
    customer,
    product_for_customer,
    product_payload
):
    response = client.post(url_products, data=product_payload)
    assert response.status_code == status.HTTP_400_BAD_REQUEST


@pytest.mark.django_db
def test_should_not_save_when_product_dont_exist_on_external_api(
    client,
    url_products,
    customer,
    product_payload
):
    product_payload['external_id'] = "9c815043-58c6-4a4e-841f-955da557ef17"
    mocked_api_call = (
        'favourites.extensions.products_api.client.'
        'ProductClient.check_product_exists_on_api'
    )
    with mock.patch(mocked_api_call, side_effect=HTTPError):  # noqa
        response = client.post(url_products, data=product_payload)
        assert response.status_code == status.HTTP_400_BAD_REQUEST


@pytest.mark.django_db
def test_should_not_save_when_payload_is_invalid(
    client,
    url_products
):
    product_payload = {}
    response = client.post(url_products, data=product_payload)
    assert response.status_code == status.HTTP_400_BAD_REQUEST
