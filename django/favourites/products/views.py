from requests import HTTPError
from rest_framework import status, viewsets
from rest_framework.pagination import LimitOffsetPagination
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response

from django.conf import settings
from django.db import IntegrityError
from django.utils.decorators import method_decorator
from django.views.decorators.cache import cache_page
from favourites.products.models import Product
from favourites.products.serializers import ProductSerializer


class ProductPagination(LimitOffsetPagination):
    default_limit = 100


class ProductViewset(viewsets.ViewSet):
    def create(self, request):
        permission_classes = (IsAuthenticated,)  # noqa
        serializer = ProductSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)

        try:
            serializer.check_product_availability_to_save(
                request.data['external_id'],
                request.data['customer']
            )
        except (HTTPError):
            return Response(
                serializer.errors,
                status=status.HTTP_400_BAD_REQUEST
            )

        try:
            serializer.save()
            return Response(
                serializer.data,
                status=status.HTTP_201_CREATED
            )
        except IntegrityError:
            return Response(
                'Customer Already favourited this product',
                status=status.HTTP_400_BAD_REQUEST
            )

    @method_decorator(cache_page(settings.CACHE_TTL))
    def list(self, request, customer_id):
        permission_classes = (IsAuthenticated,)  # noqa
        pagination = ProductPagination()
        queryset = Product.objects.filter(customer__id=customer_id)
        products = pagination.paginate_queryset(queryset, request)
        serializer = ProductSerializer(instance=products, many=True)
        return pagination.get_paginated_response(serializer.data)
