from django.db import models
from favourites.customers.models import Customer


class Product(models.Model):
    title = models.CharField(max_length=255)
    image = models.CharField(max_length=255)
    price = models.FloatField(default=0.0)
    external_id = models.UUIDField()
    review_score = models.CharField(max_length=20, null=True)
    customer = models.ForeignKey(
        Customer,
        on_delete=models.CASCADE,
        related_name='customer'
    )

    class Meta:
        constraints = [
            models.UniqueConstraint(
                fields=['external_id', 'customer'],
                name='customer_product'
            )
        ]

    @staticmethod
    def product_already_on_list(product_id, customer_id):
        try:
            Product.objects.get(
                external_id=product_id,
                customer__id=customer_id
            )
            return True
        except Product.DoesNotExist:
            return False
