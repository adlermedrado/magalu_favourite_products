import pytest
from model_mommy import mommy
from rest_framework.test import APIClient

from django.contrib.auth.models import User


@pytest.fixture
def client():
    user_data = {
        'username': 'technotronic',
        'email': 'pumpupthejam@pumpitup.com',
        'password': 'm4k3myD4y'
    }
    user = User.objects.create(**user_data)
    client = APIClient()
    client.force_authenticate(user=user)
    return client


@pytest.fixture
def url_customers():
    return '/api/v1/customers/'


@pytest.fixture
def url_products():
    return '/api/v1/products/'


@pytest.fixture
def customer():
    return mommy.make(
        'customers.Customer',
        name='Shaquille O\'Neal',
        email='shaq@nba.com'
    )


@pytest.fixture
def product_for_customer(customer):
    return mommy.make(
        'products.Product',
        title='Geladeira/Refrigerador Continental Cycle Defrost',
        image='http://challenge-api.luizalabs.com/images/3dd993ed-699f-d509-e07f-29aac25120ee.jpg',
        price=1799.0,
        external_id='4e1867f7-bf45-2962-7b0a-ba8dd378ddfe',
        review_score='4.535714',
        customer=customer
    )


@pytest.fixture
def product_payload(customer):
    return {
        "title": "Geladeira/Refrigerador Continental Cycle Defrost",
        "image": "http://img.com/img/3dd993ed-699f-d509-e07f-29aac25120ee.jpg",
        "price": 1799.0,
        "external_id": "4e1867f7-bf45-2962-7b0a-ba8dd378ddfe",
        "review_score": "4.535714",
        "customer": customer.id
    }
