from vcr import VCR


def get_vcr_instance(records_dir):
    if not records_dir:
        raise Exception('RECORDS_DIR must have a value')

    return VCR(
        record_mode='once',
        serializer='yaml',
        cassette_library_dir=records_dir,
        path_transformer=VCR.ensure_suffix('.yaml'),
        match_on=['method']
    )
