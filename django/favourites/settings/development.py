from .base import *  # noqa

DEBUG = True

DATABASE_URL = 'DATABASE_URL=postgresql://postgres:postgres@localhost:54321/postgres'
DATABASES = {
    'default': config(
        'DATABASE_URL',
        cast=db_url,
        default='sqlite://database.sqlite3'
    )
}
