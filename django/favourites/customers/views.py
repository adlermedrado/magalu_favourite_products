from rest_framework import generics, status
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response

from django.conf import settings
from django.http import Http404
from django.utils.decorators import method_decorator
from django.views.decorators.cache import cache_page
from favourites.customers.models import Customer
from favourites.customers.serializers import CustomerSerializer


class CustomerList(generics.ListCreateAPIView):
    @method_decorator(cache_page(settings.CACHE_TTL))
    def get(self, request):
        customers = Customer.objects.all()
        serializer = CustomerSerializer(customers, many=True)
        return Response(serializer.data)

    def post(self, request):
        serializer = CustomerSerializer(data=request.data)
        if serializer.is_valid() and not Customer.customer_already_exists(
                request.data['email']
        ):
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(
            serializer.errors,
            status=status.HTTP_400_BAD_REQUEST
        )


class CustomerDetail(generics.RetrieveUpdateDestroyAPIView):
    queryset = Customer.objects.all()
    serializer_class = CustomerSerializer
    permission_classes = (IsAuthenticated,)

    @method_decorator(cache_page(settings.CACHE_TTL))
    def get(self, request, pk,):
        try:
            customer = Customer.objects.get(pk=pk)
        except Customer.DoesNotExist:
            raise Http404

        serializer = CustomerSerializer(customer)
        return Response(serializer.data)
