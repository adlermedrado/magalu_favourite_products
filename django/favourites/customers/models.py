from django.db import models


class Customer(models.Model):
    email = models.EmailField(max_length=255, unique=True)
    name = models.CharField(max_length=255)

    @staticmethod
    def customer_already_exists(email):
        try:
            Customer.objects.get(email=email)
            return True
        except Customer.DoesNotExist:
            return False
